# README #

codesignIcons

### What is this repository for? ###

* Creates different types of icons for mobile platform apps.
* Saves images into a 'icons' folder
* Version 1.0

### How do I get set up? ###

* [instal **nodeJS**](http://nodejs.org)
* instal *lwip* library (npm install lwip)
* run in terminal __node__ __ iosifyImages.js__ __directoryLocation__  __imageTypeExtension__
* example: *node iosifyImages.js /Users/USER_NAME/Documents/images png*