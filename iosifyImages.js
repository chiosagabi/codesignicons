// Piece of node which will create retina and non-retina images from a @3x image type
// Will create copies of images for all .png files from a given directory

var fs 				=	require('fs');
var lwip			=	require('lwip');
var path 			=	require('path');

// get the path for the directory
// from which we must read the files
var dirPath			=	process.argv[2];


// store the filter extension 
var filterExtension	=	process.argv[3];

// read async the file
// and parse the data if no error encountered
fs.readdir(dirPath, function (err, files) {
  if (err) throw err;


	for (var i = 0; i < files.length; i++) 
	{
		// get the current extension
		var currentExtension =  path.extname(files[i])

		// if it's the same with the provided one
		// print it
		if (currentExtension == ('.' + filterExtension))
		{

			// we have a file
			var fileName	=	files[i];
			
			// create the names
			var fileNameBase	=	fileName.slice(0,-1*(filterExtension.length+1)); 
			var tretina			=	dirPath + '/icons/'+fileNameBase + '@3x.' + filterExtension;
			var retina 			=	dirPath + '/icons/'+fileNameBase + '@2x.' + filterExtension;
			var nonRetina 		=	dirPath + '/icons/'+fileNameBase +	'.'	+ filterExtension;

			// create icons folder 
			// where we'll store the images
			fs.exists(dirPath + '/icons/',function(exists)
			{
				if (!exists)
				{
					fs.mkdir(dirPath + '/icons/', function(err){
  			 		if(err) 
  			 			if (err.errno !== 47)
  			 						console.error(err);
  			 		});
				}
			});
			iosifyImage(dirPath + '/' +	fileName, tretina,		3/3);
			iosifyImage(dirPath + '/' +	fileName, retina,		2/3);
			iosifyImage(dirPath + '/' +	fileName, nonRetina,	1/3);
		}
	};
  
});

function iosifyImage(baseImage, imageName, scale)
{
	lwip.open(baseImage, function(err, image){

	if (err) throw err;
  	// define a batch of manipulations and save to disk as JPEG:
 	image.batch()
    .scale(scale)           // scale image
    .writeFile(imageName, errorHandler);

});
}

function errorHandler (err)
{
	if (err) throw err;
}

